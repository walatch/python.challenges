# insert this program in practice_files folder for it to wrok correctly (relies on Path.cwd())

from pathlib import Path

documents_path = Path.cwd() / "documents"

images_path = Path.cwd() / "images"
images_path.mkdir(exist_ok=True)


for file in documents_path.rglob("*.*"):
    if file.suffix in [".png", ".gif", ".jpg"]:
        file.replace(images_path / file.name)
