from pathlib import Path
import csv


source_path = (
    Path.home() /
    "Desktop" /
    "python-basics-exercises-master" / 
    "ch12-file-input-and-output" / 
    "practice_files" / 
    "scores.csv"
)

with source_path.open(mode="r", encoding="utf-8") as file:
    csv_reader = csv.DictReader(file)
    scores = [row for row in csv_reader]

high_scores = {}
for item in scores:
    name = item["name"]
    score = int(item["score"])

    if name not in high_scores:
        high_scores[name] = score
    else:
        if score > high_scores[name]:
            high_scores[name] = score


output_file = Path.home() / "high_scores.csv"

with output_file.open(mode="w", encoding="utf-8") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    for name in high_scores:
        row_dict = {"name": name, "high_score": high_scores[name]}
        writer.writerow(row_dict)
