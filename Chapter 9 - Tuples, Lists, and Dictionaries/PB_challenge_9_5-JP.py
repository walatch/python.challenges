import random



nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]

verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]

adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]

prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]

adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]



def poem_generator():
    """ Generates a random poem from give lists of {nouns, verbs, adjectives, prepositions, adverbs} """

    # Getting random 3 nouns, 3 verbs, 3 adjectives, 2 prepositions, 1 adverb
    
    noun1 = random.choice(nouns)
    noun2 = random.choice(nouns)
    noun3 = random.choice(nouns)

    verb1 = random.choice(verbs)
    verb2 = random.choice(verbs)
    verb3 = random.choice(verbs)

    adj1 = random.choice(adjectives)
    adj2 = random.choice(adjectives)
    adj3 = random.choice(adjectives)

    prep1 = random.choice(prepositions)
    prep2 = random.choice(prepositions)

    adv1 = random.choice(adverbs)
    

    # Checking fot duplicates and rerolling for new one if so

    while noun2 == noun1:
        noun2 = random.choice(nouns)
    while noun3 == noun1 or noun3 == noun2:
        noun3 = random.choice(nouns)

    while verb2 == verb1:
        verb2 = random.choice(verbs)
    while verb3 == verb1 or verb3 == verb2:
        verb3 = random.choice(verbs)

    while adj2 == adj1:
        adj2 = random.choice(adjectives)
    while adj3 == adj1 or adj3 == adj2:
        adj3 = random.choice(adjectives)

    while prep2 == prep1:
        prep2 = random.choice(prepositions)
        

    # Checking if adj1 starts with vowel and initializing first word variable
    
    first_word = ""
    if "oiuea".find(adj1[0]) == 1:
        first_word = "A"
    else:
        first_word = "An"


    # Generating poem

    gen_poem = (f"""{first_word} {adj1} {noun1}
     
{first_word} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}
{adv1}, the {noun1} {verb2}
the {noun2} {verb3} {prep2} a {adj3} {noun3}""")

    return gen_poem

poem = poem_generator()
print(poem)










        
