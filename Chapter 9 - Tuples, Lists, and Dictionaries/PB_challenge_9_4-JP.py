def enrollment_stats(list_of_univ):
    """Returns 2 lists with number of enrolled students and tuition fees for given list [name, enrl_stud, tuit_fees]"""

    enrolled_all = []
    tuition_all = []
    
    for n in list_of_univ:
        enrolled_all.append(n[1])
        tuition_all.append(n[2])
   
    return enrolled_all, tuition_all


def mean(values):
    """Returns mean for a given list"""
    return sum(values) / len(values)


def median(values):
    """Returns median for a given list"""
    values.sort()
    
    if len(values) % 2 == 1:
        center_index = int(len(values) / 2)
        return values[center_index]
    else:
        left_center_index = (len(values) - 1) // 2
        right_center_index = (len(values) + 1) // 2
        return mean([values[left_center_index], values[right_center_index]])
    


universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

overall = enrollment_stats(universities)

print("******************************")
print(f"Total students: {sum(overall[0]):,}")
print(f"Total tuition: $ {sum(overall[1]):,}")
print()
print(f"Student mean: {mean(overall[0]):,.2f}")
print(f"Student median: {median(overall[0]):,}")
print(f"")
print(f"Tuition mean: $ {mean(overall[1]):,.2f}")
print(f"Tuition median: $ {median(overall[1])}")
print(f"******************************")
