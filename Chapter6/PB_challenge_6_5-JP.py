def invest(amount, rate, years):
    """ // amount, rate, years //
    Calculates and prints amount of total saving for each and prints it for every year """
    for i in range(years):
        amount = amount * rate + amount
        print(f"Your savings in year {i + 1}: ${amount:.2f}")

user_amount = int(input("Provide an initial amount of $: "))
user_rate = float(input("Provide a yearly rate: "))
user_years = int(input("Provide number of years: "))

invest(user_amount, user_rate, user_years)
