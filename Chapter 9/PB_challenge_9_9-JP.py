def put_hats_on_cats(all_cats):
    """ Put hat on a cat if it doesn't have it and oterway around in a loop of 100
        Every loop you skip n+1 (n being number of last iteration) and return list
        of cats with hats"""
    cats_with_hat = []
    for n in range(1, 101):
        cat = n
        while cat < 101:
            if all_cats[cat] == "no hat":
                all_cats[cat] = "hat"
            else:
                all_cats[cat] = "no hat"
            cat = cat + n

    for m in range(1, 101):
        if all_cats[m] == "hat":
            cats_with_hat.append(m)

    return cats_with_hat





circle_of_cats = ["no hat"] * 101
print(f"Cats that have hats are : {put_hats_on_cats(circle_of_cats)}")
