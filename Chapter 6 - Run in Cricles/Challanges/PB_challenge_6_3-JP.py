def convert_cel_to_far(cel):
    """ Converting Celcius to Fahrenheit """
    far = cel * (9/5) + 32
    return far

def convert_far_to_cel(far):
    """ Converting Fahrenheit to Celcius """
    cel = (far - 32) * (5/9)
    return cel


user_far = int(input("Provide temperature in degrees F: "))
print(f"Temperature {user_far}F equals to {convert_far_to_cel(user_far):.2f}C ")

user_cel = int(input("Provide temperature in degrees C: "))
print(f"Temperature {user_cel}C equals to {convert_cel_to_far(user_cel):.2f}F ")
