  
class Animal:

    animal_fed = 0

    def __init__(self, name):
        self.name = name

    def feed(self):
        self.animal_fed = self.animal_fed + 1
        return f"You give {self.name} some favourite snacks! And {self.is_hungry()}. Hunger {self.animal_fed}. "

    def is_hungry(self):
        if self.animal_fed <= 1:
            return f"{self.name} is still hungry. Give it some snack!"
        else:
            return f"{self.name} is now happy!"

    def voice(self, sound=""):
        if sound == "":
            return f"Hi! my name is {self.name}. I'm magical and I can talk!"
        else:
            return f"{self.name} {sound}"


class Dog(Animal):
    def voice(self, sound="barks!"):
        return super().voice(sound)

    def fetch(self):
        return f"You throw a stick for {self.name}."


class Sheep(Animal):

    def voice(self, sound="bleats!"):
        return super().voice(sound)

    def sheer(self):
        if self.animal_fed > 2:
            self.animal_fed = 0
            return f"You shear {self.name} and now have some wool! Nice!"
        else:
            return f"{self.name} is not yet ready for shearing. Try feeding it first."


class Pig(Animal):
    def voice(self, sound="squeals!"):
        return super().voice(sound)

class Cow(Animal):
    def milk(self):
        if self.animal_fed > 2:
            self.animal_fed = 0
            return f"You milked {self.name} and now have bucket full of milk"
        else:
            return f"{self.name} is not yet ready for milking. Try feeding it first."
    
    def voice(self, sound="doesn't moo. It speaks! Moo! Moo, moo, moo! Mooo I say!"):
        return super().voice(sound)



# testing

# initializing instances
shaun = Sheep("Shaun")
diabolo = Cow("Secret Cow")
elmer = Pig("Elmer")
beet = Dog("Beethoven")
bugs = Animal("Bugs")

# trying out methods

print(shaun.voice())
print(shaun.feed())
print(shaun.feed())
print(shaun.sheer()) # not fed enough for sheering
print(shaun.feed())
print(shaun.sheer()) # fed enough for sheering and resetting animal_fed to 0
print(shaun.sheer()) # not fed enough for shearing
print(shaun.feed()) # testing for another loop
print(shaun.feed())
print(shaun.feed())
print(shaun.sheer())
print("")
print(diabolo.voice())
print(diabolo.feed())
print(diabolo.feed())
print(diabolo.milk()) # same as with shearing
print(diabolo.feed())
print(diabolo.milk())
print(diabolo.milk())
print(diabolo.feed())
print(diabolo.feed())
print(diabolo.feed())
print(diabolo.milk())
print("")
print(elmer.voice())
print("")
print(beet.voice())
print(beet.fetch())
print("")
print(bugs.voice())
