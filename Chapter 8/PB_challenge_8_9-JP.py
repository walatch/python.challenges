import random

def region_election(chance):
    """ Simulating elections base on a given chance (0-1) """
    if random.random() < chance: # if candidate A wins, return 1, otherwise return 0
        return 1
    else:
        return 0

num_of_sim_votings = 10_000
region1 = 0 # initialazing regions
region2 = 0
region3 = 0
region1_chance = .87 # setting chances for each region
region2_chance = .65
region3_chance = .17

elections_won = 0 # initializing counter of won elections

for n in range(num_of_sim_votings):
    result_3_regions = 0 # resetting elections score for next trial
    result_3_regions += region_election(region1_chance) # simulating result for each region
    result_3_regions += region_election(region2_chance)
    result_3_regions += region_election(region3_chance)

    if result_3_regions >= 2:
        elections_won += 1

ratio_of_wins = elections_won / num_of_sim_votings

print(f"In a simulation of {num_of_sim_votings} votings for 3 regions with poll expectations \
{region1_chance}, {region2_chance}, {region3_chance} for candidate A, \
percentage chance of his victory is {ratio_of_wins:.2%}")
