#   My variation of a challenge. How many times we will get heads after tails or tails after heads,
#   in a trial of 10,000 coin flips.
#   Why? For funsies of course. 


import random

def coin_flip():
    """ Simulated fair coin flip (0, 1)"""
    return random.randint(0, 1) # 0 for tails, 1 for heads

last_two_flips = coin_flip() # first coin flip
sequence = 0

for n in range(10_000):
    flipped_coin = coin_flip() # saving present flip for resetting latest flip
    last_two_flips += flipped_coin
    if last_two_flips == 1:
        sequence += 1
    last_two_flips = flipped_coin # resetting sequence to latest flip

print(f"Number of flips out of 10,000 for sequence of both heads and tails, in this run, is {sequence}")
