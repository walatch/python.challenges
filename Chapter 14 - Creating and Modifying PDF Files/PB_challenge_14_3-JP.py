from PyPDF2 import PdfFileWriter, PdfFileReader
from pathlib import Path


class PdfFileSplitter:
    def __init__(self, source_path):
        self.pdf_reader = PdfFileReader(source_path)
        self.writer1 = None
        self.writer2 = None

    def __str__(self):
        print("Creates object for .split and .write methods. Used for splitting PDF file at a page and saving it to two files")

        
    def split(self, breakpoint):
        """ Splits PDF at a given page number to PdfFileWriter objects <writer1 & 2> """
        self.writer1 = PdfFileWriter()

        for n in self.pdf_reader.pages[:breakpoint]:
            self.writer1.addPage(n)

        self.writer2 = PdfFileWriter()

        for n in self.pdf_reader.pages[breakpoint:]:
            self.writer2.addPage(n)


    def write(self, filename):
        """ Writes splitted PDF file to two PDF files with the same name with "_1" and "_2" at the end of file name """
        with Path(filename + "_1.pdf").open(mode="wb") as output_1:
            self.writer1.write(output_1)

        with Path(filename + "_2.pdf").open(mode="wb") as output_2:
            self.writer2.write(output_2)
















        
