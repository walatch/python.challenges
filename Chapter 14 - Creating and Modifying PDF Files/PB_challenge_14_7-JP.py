from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path

def text_in_page(page):
    return page.extractText()

source_path = (
    Path.home() / 
    "Desktop" /
    "python-basics-exercises-master/" /
    "ch14-interact-with-pdf-files" /
    "practice_files" /
    "scrambled.pdf"
    )


pdf_reader = PdfFileReader(str(source_path))
pdf_writer = PdfFileWriter()


pages = list(pdf_reader.pages)
pages.sort(key=text_in_page)

for page in pages:
    rotation_degrees = page["/Rotate"]
    if rotation_degrees != 0:
        page.rotateCounterClockwise(rotation_degrees)
    pdf_writer.addPage(page)

output_path = Path.home() / "unscrambled.pdf"
with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)
