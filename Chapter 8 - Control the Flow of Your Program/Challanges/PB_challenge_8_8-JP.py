#   My variation of a challenge. How many times we will get heads after tails or tails after heads,
#   in a trial of 10,000 coin flips.
#   Why? For funsies of course. 


import random

def coin_flip():
    """ Simulated fair coin flip (0, 1)"""
    return random.randint(0, 1) # 0 for tails, 1 for heads

flip_count = 0

for n in range(10_000):

    if coin_flip() == 0: # checking if first flip is tails
        flip_count += 1
        while coin_flip() == 0: # checking if next coin flip is also tails and incrementing coin flip counter
            flip_count += 1
        flip_count += 1 # incrementing coin flip counter if it is heads

    else:
        flip_count += 1
        while coin_flip() == 1: # checking if next coin flip is also heads and incrementing coin flip counter
            flip_count += 1
        flip_count += 1 # incrementing coin flip counter if it is tails

print(f"Ratio of heads and tails per trial is {flip_count / 10_000}")
