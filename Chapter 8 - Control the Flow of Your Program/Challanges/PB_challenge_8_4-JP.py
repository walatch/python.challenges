def factors_of_number(user_num):
    for divider in range(1, user_num + 1):
        if user_num % divider == 0:
            print(f"{user_num} is divisible by {divider}")

user_number = int(input("Enter a number for its factors: "))
factors_of_number(user_number)
