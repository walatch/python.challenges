user_guessed_number = int(input("I'm thinking of a number between 1 and 10. Guess which one."))

if user_guessed_number == 3:
    print("You win!")
else:
    print("You lose!")
